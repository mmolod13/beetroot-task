import React from 'react';
import { Switch, Route } from "react-router-dom";
import NewsList from './NewsList.jsx'
import NewsDetails from './NewsDetails.jsx'

import "../../assets/style/News/News.css";

const sessionStorageKey = "news";
const apiKey = "Ax1e8zLSUfpHVYUoQIe0GPdXQLc8x9ii";

class News extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            news: [],
        };
        this.onSetNews = this.onSetNews.bind(this);
    }

    componentDidMount() {
        const cachedNews = sessionStorage.getItem(sessionStorageKey);

        if (cachedNews) {
            this.setState({ news: JSON.parse(cachedNews) });
        } else {
            fetch(`https://api.nytimes.com/svc/mostpopular/v2/emailed/7.json?api-key=${apiKey}`)
                .then(response => response.json())
                .then(data => this.onSetNews(data?.results, sessionStorageKey));
        }
    }

    onSetNews = (result, key) => {
        sessionStorage.setItem(key, JSON.stringify(result));

        this.setState({ news: result });
    };

    render() {
        const match = this.props.match;
        const news = this.state.news;
        return (
            <div className="news">
                <div className="title">
                    <h1>New York Times</h1>
                </div>
                <div className="content">
                    <Switch>
                        <Route exact path={match.path}>
                            <NewsList
                                news={news}
                            />

                        </Route>
                        <Route path={`${match.path}/:id`} >
                            <NewsDetails
                                news={news}
                            />
                        </Route>
                    </Switch>
                </div>
            </div>
        );
    }
}

export default News;