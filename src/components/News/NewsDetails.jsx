import React from 'react';
import { useParams } from "react-router-dom";

import "../../assets/style/News/NewsDetails.css";

function NewsDetails(props) {
    let { id } = useParams();
    id = +id;

    const newsItem = props.news?.find(news => news.id === id);

    const newsTitle = newsItem?.title || "";
    const newsAbstract = newsItem?.abstract || "";
    const newsImageUrl = newsItem?.media[0]['media-metadata'][2]?.url || "";
    const newsImageCaption = newsItem?.media[0]?.caption || "";
    const newsUrl = newsItem?.url || "";
    const isNewsImage = newsItem?.media.length && newsItem?.media[0].type === 'image';

    return (
        <div className="news-details">
            <h2 className="news-title">{newsTitle}</h2>
            <div className="news-content">
                {isNewsImage ? <img v-if="isImage" src={newsImageUrl} alt={newsImageCaption} /> : ""}
                <p className="abstract">
                    {newsAbstract}
                    <br />
                    <br />
                    <a href={newsUrl} rel="noreferrer" target="_blank">Find more...</a>
                </p>

            </div>
        </div>
    );
}

export default NewsDetails;