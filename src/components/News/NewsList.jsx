import React from 'react';
import { useLocation, useHistory } from "react-router-dom";

import "../../assets/style/News/NewsList.css";

function NewsList(props) {
    const history = useHistory();
    const location = useLocation();

    const newsItems = props.news.map(news =>
        <tr key={news.id}>
            <td className="created-date">{news.published_date}</td>
            <td><p>{news.title}</p></td>
            <td className="details-button"><button onClick={() => handleTap(news.id)}>Details</button></td>
        </tr>);

    const emptyMessage = <tr><td className="empty">News list is empty</td></tr>;

    function handleTap(id) {
        history.push(`${location.pathname}/${id}`);
    }

    return (
        <div className="news-list scrollable-content">
            <table>
                <tbody>
                    {newsItems.length > 0 ? newsItems : emptyMessage}
                </tbody>
            </table>
        </div>
    );
}

export default NewsList;