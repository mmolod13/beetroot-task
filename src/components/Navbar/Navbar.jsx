import React from 'react';
import { NavLink } from 'react-router-dom';
import "../../assets/style/Navbar.css";

function Navbar(props) {
    const navItems = props.routes.map(routeObject =>
        <NavItem key={routeObject.name} route={routeObject} />);

    return (
        <nav className="nav-bar">
            <ul>
                {navItems}
            </ul>
        </nav>
    );
}

function NavItem(props) {
    return (
        <li>
            <NavLink to={props.route.to}>{props.route.name}</NavLink>
        </li>
    );
}

export default Navbar;