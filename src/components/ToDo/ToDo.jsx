import React from 'react';
import { Switch, Route } from "react-router-dom";
import ToDoList from './ToDoList.jsx'
import ToDoForm from './ToDoForm.jsx'
import ToDoDetails from './ToDoDetails.jsx'

import "../../assets/style/ToDo/ToDo.css";

const localStorageKey = "todos";

class ToDo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: [],
            newItem: {
                title: "",
                description: "",
            },
        };
        this.createTodo = this.createTodo.bind(this);
        this.handleFormChange = this.handleFormChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleChangeTodo = this.handleChangeTodo.bind(this);
        this.handleDeleteItem = this.handleDeleteItem.bind(this);
    }

    componentDidMount() {
        const todos = this.getStorageTodos();
        this.setState({ todos });
    }

    setTodos(todos) {
        this.setState({
            todos,
        });
        this.setStorageTodos(todos);
    }

    setStorageTodos(todos) {
        localStorage.setItem(localStorageKey, JSON.stringify(todos));
    }

    getStorageTodos() {
        return JSON.parse(localStorage.getItem(localStorageKey)) || [];
    }

    createTodo({ title, description }) {
        return ({
            id: new Date().getTime(),
            title,
            description,
            date: new Date(),
            status: "active",
        })
    }

    handleFormChange({ name, value = "" }) {
        this.setState({
            newItem: Object.assign({}, this.state.newItem, { [name]: value }),
        });
    }

    handleFormSubmit() {
        const todos = [this.createTodo(this.state.newItem), ...this.state.todos];
        this.setTodos(todos);
        this.setState({
            newItem: {
                title: "",
                description: "",
            }
        });
    }

    handleChangeTodo(id, obj) {
        const todoItemIndex = this.state.todos.findIndex(todo => todo.id === id);
        if (todoItemIndex !== -1) {
            let todos = [...this.state.todos];
            let item = {
                ...todos[todoItemIndex],
                ...obj,
            };
            todos[todoItemIndex] = item;
            this.setTodos(todos);
        }
    }

    handleDeleteItem(id) {
        const todos = this.state.todos.filter(todo => todo.id !== id);
        this.setTodos(todos);
    }

    render() {
        const match = this.props.match;
        const newItem = this.state.newItem;
        const todoList = this.state.todos;
        return (
            <div className="todo">
                <div className="title">
                    <h1>TO DO</h1>
                </div>
                <div className="content">
                    <Switch>
                        <Route exact path={match.path}>
                            <ToDoForm
                                item={newItem}
                                onFormSubmit={this.handleFormSubmit}
                                onFormChange={this.handleFormChange}
                            />
                            <ToDoList
                                todoList={todoList}
                                onChangeTodo={this.handleChangeTodo}
                                onDeleteItem={this.handleDeleteItem}
                            />

                        </Route>
                        <Route path={`${match.path}/:id`} >
                            <ToDoDetails
                                todoList={todoList}
                                onFormChange={this.handleChangeTodo}
                            />
                        </Route>
                    </Switch>
                </div>
            </div>
        );
    }
}

export default ToDo;