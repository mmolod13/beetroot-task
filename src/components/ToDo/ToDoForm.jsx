import React from 'react';

import "../../assets/style/ToDo/ToDoForm.css";

function ToDoForm(props) {
    function handleChange(e) {
        props.onFormChange(e.target);
    }
    function handleSubmit(event) {
        event.preventDefault();
        props.onFormSubmit();
    }

    const taskValue = props.item.title;
    const descriptionValue = props.item.description;

    return (
        <form className="todo-form" onSubmit={handleSubmit}>
            <label>
                Task name:
                    <input
                    required
                    name="title"
                    type="text"
                    value={taskValue}
                    onChange={handleChange}
                    placeholder="Enter task name"
                />
            </label>
            <label>
                Description:
                    <textarea
                    required
                    name="description"
                    value={descriptionValue}
                    onChange={handleChange}
                    placeholder="Enter task description"
                />
            </label>
            <input type="submit" value="Add" />
        </form>
    );
}

export default ToDoForm