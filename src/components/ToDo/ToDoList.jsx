import React from 'react';
import ToDoItem from './ToDoItem.jsx'

import "../../assets/style/ToDo/ToDoList.css";

function ToDoList(props) {
    const listItems = props.todoList.map(todo =>
        <ToDoItem
            key={todo.id}
            item={todo}
            onChangeTodo={props.onChangeTodo}
            onDeleteItem={props.onDeleteItem}
        />);

    const emptyMessage = <tr><td className="empty">Todo list is empty</td></tr>;

    return (
        <div className="todo-list scrollable-content">
            <table>
                <tbody>
                    {listItems.length > 0 ? listItems : emptyMessage}
                </tbody>
            </table>
        </div>
    );
}

export default ToDoList;
