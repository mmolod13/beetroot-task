import React, { useState } from 'react';
import { useParams } from "react-router-dom";
import moment from 'moment';

import "../../assets/style/ToDo/ToDoDetails.css";

function TodoDetails(props) {
    let { id } = useParams();
    id = +id;

    const [isEditing, setIsEditing] = useState(false);
    const isDisabled = isEditing ? false : true;
    const todoItem = props.todoList?.find(todo => todo.id === id);

    function handleChange(e) {
        const item = e.target;
        props.onFormChange(id, { [item.name]: item.value });
    }

    function handleSubmit(event) {
        event.preventDefault();
        setIsEditing(!isEditing);
    }

    return (
        <div className="todo-details">
            <div className="header-wrapper">
                <div className="status">Status: <span className={todoItem?.status}>{todoItem?.status}</span></div>
                <div className="date-of-creation">{moment(todoItem?.date).calendar() || ""}</div>
            </div>
            <form onSubmit={handleSubmit}>
                <label>
                    Task name:
                    <input
                        disabled={isDisabled}
                        required
                        name="title"
                        type="text"
                        value={todoItem?.title || ""}
                        onChange={handleChange}
                    />
                </label>
                <label>
                    Description:
                    <textarea
                        disabled={isDisabled}
                        required
                        name="description"
                        value={todoItem?.description || ""}
                        onChange={handleChange}
                    />
                </label>
                <input type="submit" value={isEditing ? "Save" : "Edit"} />
            </form>
        </div>
    );
}

export default TodoDetails;