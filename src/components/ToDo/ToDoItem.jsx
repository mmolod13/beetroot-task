import React from 'react';
import moment from 'moment';
import { useSwipeable } from "react-swipeable";
import { useLocation, useHistory } from "react-router-dom";

import "../../assets/style/ToDo/ToDoItem.css";

const ToDoItem = (props) => {
    const history = useHistory();
    const location = useLocation();

    const item = props.item;

    const handlers = useSwipeable({
        onTap: () => handleTap(),
        onSwipedRight: () => handleSwipedRight(),
        onSwipedLeft: () => handleSwipedLeft(),
        preventDefaultTouchmoveEvent: true,
        trackMouse: true
    });

    function handleTap() {
        history.push(`${location.pathname}/${item.id}`);
    }
    function handleSwipedRight() {
        props.onChangeTodo(item.id, { status: "completed" });
    }
    function handleSwipedLeft() {
        props.onChangeTodo(item.id, { status: "active" });
    }
    function handleDelete() {
        props.onDeleteItem(item.id);
    }

    return (
        <tr className={`todo-item ${item.status === 'completed' ? 'completed' : ''}`}>
            <td className="created-date">{moment(item.date).format('ll')}</td>
            <td className="task-name" {...handlers}><p>{item.title}</p></td>
            <td className="delete-button"><button onClick={handleDelete}>Delete</button></td>
        </tr>
    )
}

export default ToDoItem;