import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import ToDo from './components/ToDo/ToDo.jsx'
import News from './components/News/News.jsx'
import Navbar from './components/Navbar/Navbar.jsx'

import "./assets/style/App.css";

export default function App() {
  const navRoutes = [
    {
      name: "News",
      to: "/news",
    },
    {
      name: "Todo",
      to: "/todo",
    }
  ];
  return (
    <Router>
      <div className="wrapper">
        <Switch>
          <Redirect exact from="/" to="/news" />
          <Route path="/news" component={News} />
          <Route path="/todo" component={ToDo} />
        </Switch>

        <Navbar routes={navRoutes} />
      </div>
    </Router>
  );
}